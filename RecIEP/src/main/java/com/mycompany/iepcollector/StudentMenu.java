/*
 * Creates an option panel for the student name and the date.
 */
package com.mycompany.iepcollector;

/**
 *
 * @author Jacob Price
 */

import java.awt.FlowLayout;
import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;

public class StudentMenu extends JPanel{
    private JTextField day;
    private JTextField year;
    private JTextField stuName;
    private JComboBox month;
    private JComboBox student;
    private JPanel sPanel;
    private JPanel editPanel;
    private String[] toStudent = null;
    
    /**
     * Constructor.
     */
    public StudentMenu() throws IOException {
        //Gets the student list.
        RefreshStudents();
    }
    
    /**
     * getName method.
     * @return The name of the selected student.
     */
    public String getName(){
            
        return student.getSelectedItem().toString();
    }
    
    /**
     * getMonth method.
     * @return The selected month.
     */
    public String getMonth(){
            
        return month.getSelectedItem().toString();
    }
    
    /**
     * getDate method.
     * @return The input date.
     */
    public Object[] getDate(){
        
        Object[] date = {(month.getSelectedIndex()+1) + "/" +
                day.getText() + "/" + year.getText(), null, null};
            
        return date;
    }
    
    /**
     * getStudents method.
     * @return The list of students.
     */
    public JComboBox getStudents(){
            
        return student;
    }
    
    /**
     * getStuName method.
     * @return Student name from the rename panel.
     */
    public String getStuName(){
        
        return stuName.getText();
    }
    
    /**
     * StudentPanel method.
     * @return A panel for the student name and date.
     */
    public JPanel StudentPanel() {
        //Creates the edit student panel.
        sPanel = new JPanel();    
        
        sPanel.setLayout(new FlowLayout());
        
        //Add the components to the content pane.
        sPanel.add(student);
        sPanel.add(Box.createHorizontalStrut(40)); //Adds horizontal space.
        sPanel.add(month);
        sPanel.add(day);
        sPanel.add(year);
        
        return sPanel;
    }
    
    /**
     * RenamePanel method.
     * @return A panel to rename a student on file.
     */
    private JPanel RenamePanel() {
        //A field to hold the students name.
        stuName = new JTextField(15);

        //Creates the edit student panel.
        editPanel = new JPanel();
        editPanel.add(new JLabel("Rename student:"));
        editPanel.add(student);           //The current student names.
        editPanel.add(Box.createHorizontalStrut(15));   //A spacer
        editPanel.add(new JLabel("to:"));
        editPanel.add(stuName);
        
        return editPanel;
    }
    
    /**
     * RefreshStudents method.
     * Refreshes the names of the students.
     */
    private void RefreshStudents() throws FileNotFoundException, IOException {
        ArrayList<String> lines = new ArrayList<String>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("tems/file.txt"));
            String line = null;
            while ((line = reader.readLine()) != null)
                lines.add(line);   
        } 
        finally {
            reader.close();
        }
        toStudent = new String[lines.size()];
        lines.toArray(toStudent);
        
        //Refreshes the student data.
        initComponents();
    }
    
    /**
     * EditNames method.
     * Renames a student if true.
     * Deletes the student data if false.
     */
    void EditNames(Boolean truth) {
        int n;  //Holds value for n.
        if (truth == true){
            //Int n creates the dialog and holds the decision.
            n = JOptionPane.showConfirmDialog(null, RenamePanel(), "Student", JOptionPane.OK_CANCEL_OPTION);
        }
        else{
            //Int n creates the dialog and holds the decision.
            n = JOptionPane.showConfirmDialog(null, getStudents(), "Student", JOptionPane.OK_CANCEL_OPTION);
        }

        //Cancels or sends to else.
        if (n == JOptionPane.CANCEL_OPTION || n == JOptionPane.CLOSED_OPTION){
            return;
        }
        else {
            if (truth == true) {
                if (!(stuName.getText() == null) && !(stuName.getText().equals(""))) {
                    //Renames the student file.
                    File f1 = new File("stu/" + student.getSelectedItem().toString() + ".xlsx");
                    boolean b = f1.renameTo(new File("stu/" + stuName.getText() + ".xlsx"));
                    if (b == true){
                        //Display success.
                        JOptionPane.showMessageDialog
                                (null, "Student data has been renamed.");
                    }
                }
                else if (stuName.getText() == null){
                    return;
                }
                else{
                    JOptionPane.showMessageDialog
                            (null, "Student name cannot be empty.");
                    EditNames(truth);
                }  
            }
            else {
                //Deletes the student file.
                File fi = new File("stu/" + student.getSelectedItem().toString() + ".xlsx");
                boolean b = fi.delete();
                if (b == true){
                    //Display success.
                    JOptionPane.showMessageDialog
                        (null, "Student data has been deleted.");
                }
                else
                    //Display failure.
                    JOptionPane.showMessageDialog
                        (null, "Error: Unable to delete student data.");
            }
            //Try to write the read data to a temporary file.
            try {
                File inputFile = new File("tems/file.txt");
                File outputFile = new File("tems/temp.txt");
                try (BufferedReader reader = new BufferedReader(new FileReader(inputFile));
                        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {

                    //Adds a new name if boolean is true.
                    if (truth == true){
                        //Adds the new name to the file.
                        writer.write(stuName.getText());
                        writer.newLine();
                    }

                    //Reads the file data until the last line is reached.
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        //Removes student name from the data written to the temp file.
                        if (!line.equals(student.getSelectedItem().toString())) {
                            writer.write(line);
                            writer.newLine();
                        }
                    }
                }

                // Delete the original file and renames the temp file the original name.
                if (inputFile.delete()) {
                    // Rename the output file to the input file
                    if (!outputFile.renameTo(inputFile)) {
                        throw new IOException("Could not rename the temp file to 'file.txt'");
                    }
                } 
                else {
                    throw new IOException("Could not delete original input file 'file.txt'");
                }

                //Updates the student information
                try {
                    RefreshStudents();
                } catch (IOException ex) {
                    Logger.getLogger(RecIEP.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * WriteName method.
     * Adds a student to the program.
     */
    void WriteName(String fileName) {
        //Asks for a student name to add to the list.
        String studentName = JOptionPane.showInputDialog
                    ("Enter a name for the student:");

        //Imports data, or cancels if student name is empty.
        if (!(studentName == null) && !(studentName.equals(""))) {
            //Add student name to list of students.
            BufferedWriter writer;
            try {
                writer = new BufferedWriter(new FileWriter("tems/file.txt", true));
                writer.write(studentName);
                writer.newLine();
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(RecIEP.class.getName()).log(Level.SEVERE, null, ex);
            }

            //Creates a file for the student.
            try {
                //Ranames the copied file.
                Files.copy(new File(fileName).toPath(), 
                        new File ("stu/" + studentName + ".xlsx").toPath());
            }
            catch (Exception ex) {
                Logger.getLogger(RecIEP.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            //Updates the student information
            try {
                RefreshStudents();
            } catch (IOException ex) {
                Logger.getLogger(RecIEP.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            //Display success.
            JOptionPane.showMessageDialog
                    (null, "Successsfully created a file for " + studentName + ".");
        }
        else if (studentName == null){
            return;
        }
        else {
            JOptionPane.showMessageDialog
                        (null, "Student name cannot be empty.");
            WriteName(fileName);
        }
    }
       
    /**
     * initComponents method.
     * Creates items for the date and student data.
     */
    private void initComponents() {
        //Create the items.
        student = new JComboBox();
        month = new JComboBox();
        day = new JTextField();
        year = new JTextField();

        //Sets the requirements for student.
        student.setModel(new javax.swing.DefaultComboBoxModel<>(toStudent));

        //Sets the requirements for month.
        month.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] 
                { "January", "February", "March", "April", "May", "June", "July", 
                    "August", "September", "October", "November", "December" }));

        //Sets the requirements for the day text field.
        day.setText("Day");
        day.setMinimumSize(new java.awt.Dimension(40, 25));
        day.setPreferredSize(new java.awt.Dimension(40, 25));

        //Sets the requirements for the year text field.
        year.setText("Year");
        year.setMinimumSize(new java.awt.Dimension(40, 25));
        year.setPreferredSize(new java.awt.Dimension(40, 25));
    }
}