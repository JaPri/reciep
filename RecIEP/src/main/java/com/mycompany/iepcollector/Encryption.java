/*
 * Gathers data for encryption and sends it to WriteData.
 */
package com.mycompany.iepcollector;

/**
 *
 * @author Jacob Price
 */

import java.util.Arrays;
import javax.swing.JOptionPane;

public class Encryption {
    //The times[] is used to identify the column to write to.
    private Object[] times = { null, null, "8:45-8:55", "8:55-9:05", "9:05-9:15", 
        "9:15-9:25", "9:25-9:35", "9:35-9:45", "9:45-9:55", "9:55-10:05", 
        "10:05-10:15", "10:15-10:25", "10:25-10:35", "10:35-10:45", "10:45-10:55", 
        "10:55-11:05", "11:05-11:15", "11:15-11:25", "11:25-11:35", 
        "11:35-11:45", "11:45-11:55", "11:55-12:05", "12:05-12:15", 
        "12:15-12:25", "12:25-12:35", "12:35-12:45", "12:45-12:55", 
        "12:55-1:05", "1:05-1:15", "1:15-1:25", "1:25-1:35", "1:35-1:45", 
        "1:45-1:55", "1:55-2:05", "2:05-2:15", "2:15-2:25", "2:25-2:35", 
        "2:35-2:45", "2:45-2:55", "2:55-3:05", "3:05-3:15", "3:15-3:25" };

    /**
     * Constructor.
     * @param s The students name.
     * @param m The month.
     * @param bookData An array of objects to encrypt.
     */
    public Encryption(String s, String m, Object[][] bookData) throws Exception {
                    
        //Arrays to be removed from bookData.
        Object[] beh = {"Behavior",0,"8:45-8:55"};
        Object[] res = {"Response",0,"8:45-8:55"};
        Object[] not = {"Type any notes here", null, " "};
            
        //Counts for a new array length with unedited data removed.
        int numlen = 0;
        for(int a = 0; a < bookData.length; a++){
            if (!(Arrays.equals(bookData[a], beh)||
                        (Arrays.equals(bookData[a], res) || (Arrays.equals(bookData[a], not))))){
                numlen++;
            }
        }
            
        //Create array b[] of same size as numlen.
        Object b[][] = new Object[numlen][2]; 
            
        //Copies data from bookData to b[] with the unedited data removed.
        int w = 0;  //Initializes count for for b[].
        for(int a = 0; a < bookData.length; a++){
            if (Arrays.equals(bookData[a], beh)||
                        (Arrays.equals(bookData[a], res)) || (Arrays.equals(bookData[a], not))){
                }
            else{
                b[w] = bookData[a];
                w++;    //Increments b[].
            }
        }
        
        //Checks for overlapping data.
        for(int x = 1; x < b.length; x++){
            for(int a = x + 1; a < b.length; a++){
                if (b[x][0].equals(b[a][0]) && b[x][2].equals(b[a][2])){
                    JOptionPane.showMessageDialog
                            (null, "Error: Overlapping data!");
                    return;
                }
            }
        }
            
        //Creates a new array for encrypted data.    
        Object cr[][] = new Object[b.length][42];
        
        //Encrypts the data based on datatype.
        //Int i represents main array, int y represents child array.
        for(int i = 0; i < b.length; i++){
            for (int y = 0; y < cr[i].length; y++){
                //Encrypts the data writing to cell 0 in excel.
                if ((y == 0) && (!(b[y] == null))){
                    cr[i][y] = StringEnc(b[i][y].toString()); 
                }
                //Encrypts the rating and places it in the column of times[y].
                else if ((i > 0) && b[i][2].equals(times[y])){
                    //Encrypts integer data.
                    cr[i][y] = IntEnc(Integer.parseInt(b[i][1].toString()));
                }
                //Pads excel with null values to space between time[y].
                else
                    cr[i][y] = null;
            }
        }
        
        //Compares matching rows and combines data.
        //n and v are to compare rows of data, r is used for the columns.
        for(int n = 0; n < cr.length; n++){
            for (int r = 0; r < cr[n].length; r++){
                for (int v = n + 1; v < cr.length; v++){
                
                    //Writes data to the same line if the first cells of a dataset match another.
                    if (cr[n][0].equals(cr[v][0])) {
                        //Keeps cell if it isn't null.
                        if (!(cr[n][r] == null)){
                            cr[n][r] = cr[n][r];
                        }
                        //Adds the matching row data and mark the row for deletion.
                        else if (!(cr[v][r] == null)){
                            cr[n][r] = cr[v][r];
                    
                            //Marks a row for deletion, and stops it from being compared again. 
                            Object[] deleter = {"delete"};
                            cr[v] = deleter;
                        }
                        //If both values are null then data stays the same (null).
                        else if (cr[n][r] == null && cr[v][r] == null){
                            cr[n][r] = cr[n][r];
                        }
                    }
                }
            }
        }
        
        //Counts for a new array length with deleted items removed.
        int del = 0;
        for(int n = 0; n < cr.length; n++){
            if (!cr[n][0].equals("delete"))
                del++;
            }
        
        //Create array toWrite[] of same size as del.
        Object toWrite[][] = new Object[del][42];
        
        //Copies data from cr to toWrite[] with deleted rows removed.
        int z = 0;  //Initializes count for for b[].
        for(int a = 0; a < cr.length; a++){
            if (cr[a][0].equals("delete")){
                }
            else{
                toWrite[z] = cr[a];
                z++;    //Increments b[].
            }
        }

        //Sends data out for writing.
        WriteData test = new WriteData(s + ".xlsx", m, toWrite);
        
        //Display success.
                    JOptionPane.showMessageDialog
                            (null, "Data successfully saved to file!");
    }
    
    /**
     * Constructor.
     */
    public Encryption(){
        //Null constructor.
    }
    
    /**
     * StringEnc method
     * @return The encrypted string.
     */
    static String StringEnc(String str){
        //Encrypts string data.
        StringBuilder sb = new StringBuilder();
        for (int a = 0; a < str.length(); a++){
            sb.append((char)(str.charAt(a)+10));
        }
        //Converts from StringBuilder object to string.
        return sb.toString();
    }
    
    /**
     * IntEnc method
     * @return The encrypted integer.
     */
    static Integer IntEnc(int num){
        return ((num + 3) * 7);
    }
}