/*
 * Runs the password protection and security question.
 */
package com.mycompany.iepcollector;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Jacob Price
 */

public class Password extends JPanel{
    private File file = new File("tems/sec.xlsx");  //The file to search for.
    private Encryption encry = new Encryption();    //Reference encryption.
    private Decryption decry = new Decryption();    //Reference decryption.
    
    /**
     * Constructor.
     */
    public Password() throws IOException, InvalidFormatException, GeneralSecurityException {
        //Check for existence of the file.
        if (!file.exists()) {
            //Creates a password.
            PassCreate();
        }
        else {
            //Asks from password confirmation.
            CheckPassword();
        }  
    }
        
    /**
     * CheckPassword method.
     * Asks the user to enter a password.
     */
    private void CheckPassword() throws IOException, InvalidFormatException, GeneralSecurityException {
        //Creates a panel for password entry
        JPanel panel = new JPanel();
        JLabel label = new JLabel("Enter a password:");
        JPasswordField passcode = new JPasswordField(15);
        panel.add(label);
        panel.add(passcode);

        //Int p creates the dialog and holds the decision.
        int p = JOptionPane.showConfirmDialog(null, panel, "Password Creation", JOptionPane.OK_CANCEL_OPTION);
        
        //Cancels or sends to else.
        if (p == JOptionPane.CANCEL_OPTION || p == JOptionPane.CLOSED_OPTION){
            System.exit(0);
        }
        else {
            //Sends the password for encryption.
            String enc = encry.StringEnc(new String(passcode.getPassword()));

            //Create Workbook instance holding reference to .xlsx file.
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            
            //Get the password from file.
            XSSFSheet sheet = workbook.getSheetAt(1);
            Row row = sheet.getRow(1);
            Cell cell = row.getCell(0);
            
            workbook.close();
            
            //Checks if the passwords match.
            if (!cell.toString().equals(enc)){
                //Asks about resetting the password.
                int n = JOptionPane.showConfirmDialog(null, "Wrong password. Would you like to reset your password?", null, JOptionPane.YES_NO_OPTION);
                
                if (n == JOptionPane.YES_OPTION){
                    //Begins password resetting process.
                    PassReset();
                }
                else if (n == JOptionPane.NO_OPTION){
                    //Asks for password again.
                    CheckPassword();
                }
                else {
                    System.exit(0);
                }
            }
        }
    }
    
    /**
     * PassReset method.
     * Resets the password.
     */
    private void PassReset() throws IOException, InvalidFormatException, GeneralSecurityException {
        //Create Workbook instance holding reference to .xlsx file.
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        
        //Get the security question from file.
        XSSFSheet sheet = workbook.getSheetAt(1);
        Row row = sheet.getRow(3);
        Cell cell = row.getCell(0);

        //Decrypts the question.
        String PassX = decry.StringDec(cell.toString());

        //Asks user the security question.
        String securityQ = JOptionPane.showInputDialog(PassX);
        
        if (securityQ != null) {
            //Encrypts the users answer.
            String sQ = encry.StringEnc(securityQ);

            //Gets the correct security answer from file.
            sheet = workbook.getSheetAt(1);
            row = sheet.getRow(2);
            cell = row.getCell(0);
            String secure = cell.toString();

            workbook.close();

            //Checks if the answer is correct.
            if (sQ.equalsIgnoreCase(secure)){
                //Recreates the password.
                PassCreate();
                }
            else{
                //Display failure.
                JOptionPane.showMessageDialog(null, "Your answer did not match the security question.");
                PassReset();
            } 
        }
        else {
            System.exit(0);
        }   
    }
    
    /**
     * PassCreate method
     * Creates the password and security question.
     */
    private void PassCreate() throws IOException, InvalidFormatException, GeneralSecurityException {
        //Shows the panel for password creation.
        CreatePasswordPanel passPanel = new CreatePasswordPanel();
        int n = JOptionPane.showConfirmDialog(null, passPanel, "Password Creation", JOptionPane.OK_CANCEL_OPTION);

        //Cancels or sends to else.
        if (n == JOptionPane.CANCEL_OPTION || n == JOptionPane.CLOSED_OPTION){
            System.exit(0);
        }
        else if (n == JOptionPane.OK_OPTION){
            //Checks that the passwords match.
            if (!passPanel.getPass1().equals(passPanel.getPass2())){
                //Display failure.
                JOptionPane.showMessageDialog(null, "Passwords don't match.");
                
                //Return to PassCreate();
                PassCreate();
            }
            else {
                //Deletes the old password.
                file.delete();

                //Creates the new password.
                try {
                    //Copies the template file.
                    Files.copy(new File("tems/add.xlsx").toPath(), 
                        new File ("tems/sec.xlsx").toPath());
                }
                catch (Exception ex) {
                    Logger.getLogger(RecIEP.class.getName()).log(Level.SEVERE, null, ex);
                }

                //Encrypts the password.
                Object[] encPass = {encry.StringEnc(passPanel.getPass1()), null};

                //Encrypts the security answer.
                Object[] encAns = {encry.StringEnc(passPanel.getAns()), null};

                //Encrypts the security question.
                Object[] encSec = {encry.StringEnc(passPanel.getSec()), null};

                //Creates a nested array for the objects.
                Object[][] arr = {encPass, encAns, encSec};

                //Write data to file.
                WriteData pass = new WriteData("tems/sec.xlsx", "January", arr);

                //Display success.
                JOptionPane.showMessageDialog
                    (null, "Password saved!");
            }
        }
    }
}