/*
 * Runs the Record IEP (RecIEP) program.
 */
package com.mycompany.iepcollector;

/**
 *
 * @author Jacob Price
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class RecIEP extends JFrame{
    private StudentMenu student = new StudentMenu();//Creates the student menu.
    private BehaviorsPanel behaviors;   //To reference the Behaviors panel.
    private ResParPanel resPar;         //To reference the Response panel.
    private JPanel buttonPanel;         //To hold the save button.
    private JButton saveButton;         //To save data to the file.
    private JButton openButton;         //To open the file.
    private JMenuBar menuBar;           //To create the menu bar.
    private JMenu f, s;                 //To reference menu options.
    private JMenuItem f1, f2, f3, f4, s1, s2, s3;
    private JTextArea notes;
  
    /**
     * Main method.
     */
    public static void main(String[] args) throws IOException, InvalidFormatException, GeneralSecurityException {
        //Gets password from user.
        new Password();
        
        //Runs the program.
        new RecIEP();
    }
    
    /**
     * Constructor.
     */
    public RecIEP() throws IOException{
        //Set the window title.
        setTitle("RecIEP");
        
        //Specify what happens when the close button is clicked.
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Create a BorderLayout manager.
        setLayout(new BorderLayout());
        
        //Create the panels.
        buildPanel();   //The button panel.
        buildMenu();    //The menu buttons.
        behaviors = new BehaviorsPanel();
        resPar = new ResParPanel();
        notes = new JTextArea();
        
        //Set the requirements for the notes text field.
        notes.setText("Type any notes here");
        notes.setMinimumSize(new java.awt.Dimension(100, 100));
        notes.setPreferredSize(new java.awt.Dimension(100, 100));
        notes.setBorder(BorderFactory.createTitledBorder("Notes"));
        
        //Add the components to the content pane.
        add(buttonPanel, BorderLayout.NORTH);
        add(behaviors, BorderLayout.WEST);
        add(resPar, BorderLayout.EAST);
        add(notes, BorderLayout.SOUTH);
        
        //Pack and display the window.
        pack();
        setVisible(true);
    }
    
    /**
     * The buildPanel method builds the save and open button panel.
     */
    private void buildPanel(){
        //Create a panel for the save button.
        buttonPanel = new JPanel();
        
        //Create the buttons.
        saveButton = new JButton("Save to File");
        openButton = new JButton("Open a File");
        
        //Add action listener to the buttons.
        saveButton.addActionListener(new SaveButtonListener());
        openButton.addActionListener(new OpenButtonListener());
                
        //Add the save button to the button panel.
        buttonPanel.add(Box.createVerticalStrut(40)); //Adds vertical space.
        buttonPanel.add(saveButton);
        buttonPanel.add(Box.createHorizontalStrut(200)); //Adds horizontal space.
        buttonPanel.add(openButton);
        buttonPanel.add(Box.createVerticalStrut(40)); //Adds vertical space.
    }
    
    /**
     * The buildMenu method builds the menu options.
     */
    private void buildMenu(){
        //Creates the menu bar.
        menuBar = new JMenuBar();
  
        //Creates the menu items.
        f = new JMenu("File");
        s = new JMenu("Student");
  
        //Creates the menu options.
        f1 = new JMenuItem("Open");
        f2 = new JMenuItem("Save");
        f3 = new JMenuItem("Import");
        f4 = new JMenuItem("Exit");
        s1 = new JMenuItem("Add");
        s2 = new JMenuItem("Remove");
        s3 = new JMenuItem("Edit");
  
        //Adds all items to the menu bar.
        f.add(f1); 
        f.add(f2); 
        f.add(f3); 
        f.add(f4); 
        s.add(s1);
        s.add(s2); 
        s.add(s3);
        menuBar.add(f);
        menuBar.add(s);
        
        //Add action listeners to the menu items.
        f1.addActionListener(new OpenButtonListener());
        f2.addActionListener(new SaveButtonListener());
        f3.addActionListener(new ImportButtonListener());
        f4.addActionListener(new ExitButtonListener());
        s1.addActionListener(new AddButtonListener());
        s2.addActionListener(new RemoveButtonListener());
        s3.addActionListener(new EditButtonListener());
  
        //Add the menu to frame. 
        setJMenuBar(menuBar);
    }
    
    /**
     * OpenButtonListener is an action listener class for the Open button.
     */
    private class OpenButtonListener implements ActionListener{
        /**
         * The actionPerformed method executes when 
         * the user clicks on the Open a File button.
         * @param e The event object.
         */
        public void actionPerformed(ActionEvent e) { 
            //Int n creates the dialog and holds the decision.
            int n = JOptionPane.showConfirmDialog(null, student.getStudents(), "Student", JOptionPane.OK_CANCEL_OPTION);
            
            //Cancels or sends to else.
            if (n == JOptionPane.CANCEL_OPTION || n == JOptionPane.CLOSED_OPTION){
                return;
            }
            else {
                try {
                    //Opens the encrypted file and sends it to decryption.
                    ReadData re = new ReadData("stu/" + student.getName(), 
                            "/desktop/" + student.getName());
                } catch (IOException | GeneralSecurityException | InvalidFormatException ex) {
                    Logger.getLogger(RecIEP.class.getName()).log(Level.SEVERE, null, ex);
                }
                //Display success.
                JOptionPane.showMessageDialog
                        (null, "Data successfully decrypted and saved to desktop!");
            }
            
            //Opens the decrypted file.
            if (Desktop.isDesktopSupported()) {
                try {
                    String userHomeFolder = System.getProperty("user.home");
                    File myFile = new File(userHomeFolder + "/desktop/" + student.getName() + "dec.xlsx");
                    Desktop.getDesktop().open(myFile);
                } 
                catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
        }
    }
    
    /**
     * SaveButtonListener is an action listener class for the Save button.
     */
    private class SaveButtonListener implements ActionListener{
        /**
         * The actionPerformed method executes when 
         * the user clicks on the Save to File button.
         * @param e The event object.
         */
        public void actionPerformed(ActionEvent e) {
            //Int n creates the dialog and holds the decision.
            int n = JOptionPane.showConfirmDialog(null, student.StudentPanel(), "Student", JOptionPane.OK_CANCEL_OPTION);
            
            //Cancels or sends to else.
            if (n == JOptionPane.CANCEL_OPTION || n == JOptionPane.CLOSED_OPTION){
                return;
            }
            else {
                try {
                    //Creates an array for the notes.
                    Object[] note = {notes.getText(), null, " "};
                    
                    //Gets a nested array to send to encryption.
                    Object[][] data = 
                        {student.getDate(), note, behaviors.getBehaviors1(),
                        behaviors.getBehaviors2(), behaviors.getBehaviors3(),
                        behaviors.getBehaviors4(), behaviors.getBehaviors5(), 
                        behaviors.getBehaviors6(), behaviors.getBehaviors7(),
                        behaviors.getBehaviors8(), behaviors.getBehaviors9(),
                        behaviors.getBehaviors10(), resPar.getRes1(),
                        resPar.getRes2(), resPar.getRes3(), resPar.getRes4(), 
                        resPar.getRes5(), resPar.getRes6(), resPar.getRes7(),
                        resPar.getRes8(), resPar.getRes9(), resPar.getRes10() };
                    
                    //Runs the WriteData class with the following arguments.
                    Encryption saver = new Encryption
                        ("stu/" + student.getName(), student.getMonth(), data);
                } catch (Exception ex) {
                    Logger.getLogger(RecIEP.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    /**
     * ImportButtonListener is an action listener class for the Import button.
     */
    private class ImportButtonListener implements ActionListener{
        /**
         * The actionPerformed method executes when 
         * the user clicks on the Import menu button.
         * @param e The event object.
         */
        public void actionPerformed(ActionEvent e) {  
            //Creates an object to select the import file.
            JFileChooser chooser = new JFileChooser();
            
            //Filters the selectable files.
            FileNameExtensionFilter filter = new FileNameExtensionFilter(
                    "Excel", "xlsx", "xls");
            chooser.setFileFilter(filter);
            
            //Returns the selected file.
            int returnVal = chooser.showOpenDialog(null);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                //Writes the chosen file to the student data.
                student.WriteName(chooser.getSelectedFile().getAbsolutePath());
            }
        }
    }
    
    /**
     * ExitButtonListener is an action listener class for the Exit button.
     */
    private class ExitButtonListener implements ActionListener{
        /**
         * The actionPerformed method executes when 
         * the user clicks on the Exit menu button.
         * @param e The event object.
         */
        public void actionPerformed(ActionEvent e) {        
            //Exits the program.
            System.exit(0);
        }
    }
    
    /**
     * AddButtonListener is an action listener class for the Add button.
     */
    private class AddButtonListener implements ActionListener{
        /**
         * The actionPerformed method executes when 
         * the user clicks on the Add student button.
         * @param e The event object.
         */
        public void actionPerformed(ActionEvent e) {
            //Copies the template and writes file to student data.
            student.WriteName("tems/add.xlsx");
        }
    }
    
    /**
     * RemoveButtonListener is an action listener class for the Remove button.
     */
    private class RemoveButtonListener implements ActionListener{
        /**
         * The actionPerformed method executes when 
         * the user clicks on the Remove student button.
         * @param e The event object.
         */
        public void actionPerformed(ActionEvent e) {
            /*Removes the students name and file.
            *
            *Sending false as an argument will tell EditNames to remove the
            *student files. True will keep the file but change the student name.
            */
            student.EditNames(false);
            
        }
    }
    
    /**
     * EditButtonListener is an action listener class for the Edit button.
     */
    private class EditButtonListener implements ActionListener{
        /**
         * The actionPerformed method executes when 
         * the user clicks on the Edit student button.
         * @param e The event object.
         */
        public void actionPerformed(ActionEvent e) {
            /*Edits the students name and file name.
            *
            *Sending true as an argument will tell EditNames to keep the file 
            *but change the student name. False will remove the student files.
            */
            student.EditNames(true);
        }
    }
}