/*
 * Reads the data from an encrypted student file.
 */
package com.mycompany.iepcollector;

/**
 *
 * @author Jacob Price
 */

import java.util.*;
import java.io.*;
import java.security.GeneralSecurityException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*; 
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFSheet; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadData {
    //References the month that the data is reading from.
    private String[] month = { "January", "February", "March", "April", "May", "June", "July", 
                "August", "September", "October", "November", "December" };
    
    /**
     * Constructor.
     * @param s The students name.
     * @param d The decrypted file name.
     */
    public ReadData(String s, String d) throws IOException, GeneralSecurityException, InvalidFormatException {
        //The file to read from.
        FileInputStream file = new FileInputStream(new File(s + ".xlsx"));

        //Create Workbook instance holding reference to .xlsx file.
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        //Tells the workbook to read blank cells between data.
        //This is necessary to keep data in the right location.
        workbook.setMissingCellPolicy(MissingCellPolicy.RETURN_BLANK_AS_NULL);

        //Send each sheet to Decryption.
        for(int p = 1; p < 13; p++){
            //Get first/desired sheet from the workbook.
            XSSFSheet sheet = workbook.getSheetAt(p);

            //An arraylist to hold the data from each row.
            ArrayList<ArrayList<Object>> list = 
                     new ArrayList<ArrayList<Object>>();

            //Reads the data from each cell.
            for (int rn=sheet.getFirstRowNum(); rn<=sheet.getLastRowNum(); rn++) {
                Row row = sheet.getRow(rn);

                //An arraylist to hold the cell data.
                ArrayList<Object> y = new ArrayList<Object>();

                //Adds cell data to y.
                for (int cn=0; cn<row.getLastCellNum(); cn++) {
                    Cell cell = row.getCell(cn);
                    if (cell == null) {
                        y.add(null);
                    }
                    else {
                        //Gets the celltype from excel.
                        CellType type = cell.getCellType();
                        if (type == CellType.NUMERIC){
                            //Converts from double to int.
                            double data = cell.getNumericCellValue();
                            int x = (int)data;
                            y.add(x);
                        }
                        else if (type == CellType.STRING){
                            y.add(cell.getRichStringCellValue().toString());
                        }
                    }
                }
                //Adds all of the data from the row to list.
                list.add(y);
            }
            
            //Create an array the same size as numlen.
            Object[][] toDecrypt = new Object[list.size()][47];

            //Converts data from arraylist to object array.
            for(int i = 0; i < list.size(); i++){
                for(int a = 0; a < list.get(i).size(); a++){
                    if(list.get(i).get(a) instanceof String){
                        toDecrypt[i][a] = list.get(i).get(a).toString();
                    }
                    else if(list.get(i).get(a) instanceof Integer){
                        toDecrypt[i][a] = Integer.parseInt(list.get(i).get(a).toString());
                    }
                    else toDecrypt[i][a] = null;
                }
            }
            //Sends the data out for decryption.
            Decryption dec = new Decryption(s, d, month[p - 1], toDecrypt);    
        }
        workbook.close();
        file.close();
    }
}