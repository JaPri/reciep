/*
 * Writes and formats the data to the necessary files.
 */
package com.mycompany.iepcollector;

/**
 *
 * @author Jacob Price
 */

import javax.swing.*;
import java.io.*;
import java.security.GeneralSecurityException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteData {
    
    /**
     * Constructor.
     * @param s The students name.
     * @param m The month.
     * @param b The encrypted/decrypted data.
     */
    public WriteData(String s, String m, Object[][] b) 
            throws InvalidFormatException, GeneralSecurityException, 
            FileNotFoundException, IOException {
        String fileName = (s);      //Holds the name of the file.
        XSSFWorkbook workbook;      //Holds the workbook.
        
        File file = new File(fileName);     //The file to search for.
        
        //Check for existence of the file.
        if (!file.exists()) {
            //Display error.
            JOptionPane.showMessageDialog
                        (null, "Error: File not found!");
        }
        
        //Add the collected data to the file.
        try {
            //Create Workbook instance holding reference to .xlsx file.
            workbook = new XSSFWorkbook(new FileInputStream(fileName));
            
            //Gets sheet for the selected month.
            Sheet sheet = workbook.getSheet(m);      
            
            //Finds the next empty row.
            int rowCount = sheet.getLastRowNum();
            
            //Writes each row of data to the sheet.
            for (Object[] aBook : b) {
                Row row = sheet.createRow(++rowCount);
                
                //The first cell to write to.
                int cellCount = 0;
                 
                //Writes data based on data type.
                for (Object field : aBook) {
                    Cell cell = row.createCell(cellCount++);
                    
                    if (field instanceof String)
                        cell.setCellValue((String) field); 
                    
                    else if (field instanceof Integer)
                        cell.setCellValue((Integer) field);
                    
                    else if (field instanceof Boolean){
                        cell.setCellFormula("AVERAGE(C" + (rowCount + 1)
                                + ":AP" + (rowCount + 1) + ")");
                    }
                    else cell.setBlank();
                }
            }

            //Resizes the first column to fit the strings.
            sheet.autoSizeColumn(0);
            
            //Recalculates the formulas.
            workbook.setForceFormulaRecalculation(true);
                
            FileOutputStream outputStream = new FileOutputStream(fileName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();            
        } 
        catch (IOException | EncryptedDocumentException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());  
        } 
    }
}