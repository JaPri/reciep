/*
 * Gathers data for decryption and sends it to WriteData.
 */
package com.mycompany.iepcollector;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/**
 *
 * @author Jacob
 */

public class Decryption {
     
    /**
     * Constructor.
     * @param s The students name.
     * @param d The decrypted file name.
     * @param m The month.
     * @param arr The data to decrypt.
     */
    public Decryption(String s, String d, String m, Object[][] arr) throws IOException, 
            InvalidFormatException, GeneralSecurityException{
        //Gets the desktop filepath.
        String userHomeFolder = System.getProperty("user.home");
        
        //Creates a string for the desktop filepath.
        String desk = (userHomeFolder + d + "dec.xlsx");
        
        File file = new File(desk);     //The file to search for.
        
        //Check for existence of the file.
        if (!file.exists()) {
            //Creates a new file for the student if it doesn't already exist.
            try {
            //Copies from the template file.
            Files.copy(new File("tems/Template.xlsx").toPath(), 
                    new File (desk).toPath());
            }
            catch (Exception ex) {
                Logger.getLogger(RecIEP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         
        //Creates an array the same size as arr.            
        Object toWrite[][] = new Object[arr.length][47];
         
        //Decrypts the data based on datatype and copies it to toWrite[].
        //Int i represents main array, int y represents child array.
        for(int i = 0; i < arr.length; i++){
            for (int y = 0; y < arr[i].length; y++){
                if(arr[i][y] instanceof String){
                    //Decrypts string data.
                    toWrite[i][y] = StringDec(arr[i][y].toString()); 
                }
                else if (arr[i][y] instanceof Integer)
                    //Decrypts integer data.
                    toWrite[i][y] = IntDec(Integer.parseInt(arr[i][y].toString()));
                else if (y == 42){
                    //Used by writeData to create a row average at column# 42.
                    toWrite[i][y] = true;
                }
                else toWrite[i][y] = null;
            }
        }    
        //Sends data out for writing.
        WriteData test = new WriteData(desk, m, toWrite);
    }
    
    /**
     * Constructor.
     */
    public Decryption(){
        //Null constructor.
    }
    
    /**
     * StringDec method
     * @return The decrypted string.
     */
    static String StringDec(String str){
        //Encrypts string data.
        StringBuilder sb = new StringBuilder();
        for (int a = 0; a < str.length(); a++){
            sb.append((char)(str.charAt(a)-10));
        }
        //Converts from StringBuilder object to string.
        return sb.toString();
    }
    
    /**
     * IntDec method
     * @return The decrypted integer.
     */
    static Integer IntDec(int num){
        return ((num / 7) - 3);
    }
}